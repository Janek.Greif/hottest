# Components
Current ui-frameworks use components as their building blocks. React, Svelte, Solid-js, Flutter, Jetpack Compose, SwiftUi, ... all use components as their building blocks (Flutter calls them widgets and Jetpack Compose call them composables). Components differ to other architectures such as MVC and MVVM, that each components handles ui as well as state. React, Svelte and Solid-js also provide a mechanism for state derivation, however these are only unidirectional. Flutter and Jetpack Compose do not provide help for state derivation out of the box, but both allow for extern state management libraries that offer such solutions. 

Since HotDrink does not really with the ui-side of components I used Solid-js components but with HotDrink's property constraint model instead of Solid-Js' fine-grained reactivity. _(basic-setup)_

I do not know if HotDrink was supposed to be suitable for a component architecture, but I think it should be at least thought about.

## HotDrink Components
HotDrink does have its own components, where variables and constraints are defined. Each Solid-Js Components has its own HotDrink Component and using JSX-Properties a Variable is shared between them. This produces an error `Construction constraint with a foreign variable reference` but it still worked as expected. _(solid-and-hot-drink-components)_

## HotDrink DSL
Using the tagged template literal api for component definition, seems like an obvious choice, but there needs to be a way to use external variables inside it to make it truly useful. Another approach would be to define the ui inside a `.hd` file to support the single-file-component model (like Svelte). 

I did not use the DSL from HotDrink for my test. Some things I did are not actually allowed in HotDrink and after writing some wrappers around the HotDrink-API I had a relatively pleasant api to work with. Also I was unable to use interpolated arrow functions inside the DSL.

## Cleanup
There exists a `removeComponent` function that should be called `onCleanup`, otherwise the constraints created by the component are still active. Solid-Js' components do this automatically, but even if I need a manual call for HotDrink, the functionality seems to be there. _(constraints-cleanup)_

## Access to out-variables
Currently a method gets only access to the in-variables. It would open up some patterns, if the method can also access the out-values. _(access-to-out-values)_ has a example of using out-values in its method computation. The example does work, but what would really help here is a way to control what method is used to enforce the constraint.

## Trivial fulfillment
It may be useful sometimes to allow methods to 'fail' their computation by trivially fulfilling the constraint they are part of. This can be simulated if the method has access to the out-variables, by returning them unmodified. The constraint-system has to recognize then, that the value did not change.

## Algebraic constraints 
It may look like this:
```
var a, b, c;


constraint eq {
    a == b;
}

// in terms of methods:
constraint eq {
    m1(a -> b) => a;
    m2(b -> a) => b;
}

constraint three-way {
    a=b/c;
}

// in terms of methods:
constraint three-way {
    m1(b, c -> a) => b/c;
    m2(a, c -> b) => a*c;
    m3(a, b -> c) => b/a;
}
```

## Effects
Effects are functions that react to state changes (eg rendering or custom logic). Since effect don't produce values, they cannot be part of the constraints-concepts. Solid-Js' effects are relative simple: The function is executed, and any read to a signal subscribes that function to that signal. 

In HotDrink there is only a way to subscribe to a single variable. But effects are what give a reactive system its use.

```ts
const a: Variable<number> = ...;
const b: Variable<string> = ...;
hd.effect(() => {
    const age = a.get();
    const name = b.get();
    console.log("age:", age, "name", name);
});
```

If either `a` or `b` changes then the effect is re-run.

## Nested Lists and Objects
Nested Lists and Objects are not (yet?) supported.

### Object
In Solid-Js the fields of an object however deep are not tracked. A change is only detected if the entire object is replaced. If a nested field should be tracked as well, then it also needs to be a signal. Something similar can also be done for HotDrink, but how should nested Variables be handled? If a constraint exist between variables that are nested in another variable, then there must be a way to re-create the constraint depending of the outer variable.

Effect can be used to solve this problem. Inside an effect a variable is read and a constraint is created, which can use the value of the variable and since the effect is re-run if the variable changed, the constraint is also recreated.

Maybe it could look like that.
```ts
interface User {
    name: Variable<string>,
    age: Variable<number>,
    address?: {
        city: Variable<string>,
        street: Variable<string>,
        street_number: Variable<number>,
    }
}


const obj = Variable<User>({
    name: Variable("ME"),
    age: Variable(42),
    address: { // address is not a Variable, and may be null
        city: Variable("Springfield");
        street: Variable("Evergreen Terrace"),
        street_number: Variable(742),
    }
});
```

In a hypothetical HotDrink syntax.
```
extract { name, address?: { city }} of obj {
    constraint test {
        m1(obj, name, city -> c) => ...;
    }
}

```

As typescript api. (This is written like an automated transpilation from the HotDrink syntax)
```ts
hd.effect(() => {
    const temp1 = obj.get();
    const name = temp1.name;
    if (!name) {
        return;
    }
    const address = temp1.address;
    let city = null;
    if (address) {
        city = address.city;
        if (!city) {
            return;
        }
    }
    const constraint = createConstraint("test");
    constraint.addMethod(..., [obj, name, city], c);
    comp.addConstraint(constraint);
});
```

`extract` is used to deconstruct a generic object to extract inner variables. The inner-most variables are not read and do not cause a subscription from the effect. However all read variables in between do cause a subscription. The created constraint is also connected to the effect, to identify the old constraint to either replace to remove the constraint.

Adding the constraint to the component inside an effect needs to be mindful of multiple executions of that effect, re-adding means replacing, and not adding means removing.

Reading from a nested property inside a method, should probably not be allowed. It may break the constraint system.

Is this a good idea? Maybe?

### Lists
There are a lot of similarities between lists and objects, but I found two big differences:
1. The index needs to be a variable, so that permutations can be observed if that index changes. In Solid-Js, when iterating over a signal containing an array, the index provided in the callback is also a signal.
2. When creating a constraint, you can either have one constraint using all variables nested inside the array as input/output, or multiple constraints that take singular element.

```ts
interface Reaction {
    emoji: string,
    count: Variable<number>,
}

interface Message {
    text: string,
    reactions: Variable<Reaction[]>
}

interface Room {
    messages: Variable<Message[]>
}

const rooms = Variable<Room>({
    messages: Variable([
        {
            text: "Welcome to this room",
            reactions: Variable([
                {
                    emoji: "👍",
                    count: Variable(4)
                },
                {
                    emoji: "🚀",
                    count: Variable(2)
                }
            ]),
            reaction_text: Variable("")
        }
    ])
})
```

```
for message of room.messages {

    extract { reactions, reaction_text } of message {

        constraint test {

            extract { count }[] of reactions {

                // count is a array of variables, where each variable is used as input. It also allows access of the nested variable inside reactions, which normally would not be allowed.
                generate(reactions, count -> reaction_text) => reactions.map(r => r.emoji.repeat(r.count)).join('\n');

                // the new reactions do not contain variables, instead they provide a placeholder without data, then separately (according to the output variables of the method) a second array with the value for the counts is returned.
                parse(reaction_text -> reactions, count) => {
                    const emojisAndCount: [string, number][] = reaction_text.split('\n').map(line => findAndCountProminentEmoji(line));
                    const emojis = emojisAndCount.map(e => { return { emoji: e[0], count: VariablePlaceholder() }});
                    const counts = emojisAndCount.map(e => e[1]);
                    return [emojis, counts];
                };
            }
        }
    }
}
```

```ts
const component: hd.Component = ...;
hd.For(room.get().messages, (message) => {
    const reactions = message.reactions;
    const reaction_text = message.reaction_text;
    const count = reactions.get().map(reaction => reaction.count);

    const constraint = createConstraint("test");
    constraint.addMethod((reactions, count) => reactions.map(r => r.emoji.repeat(r.count)).join('\n'), [reactions, count], [reaction_text], "generate");
    constraint.addMethod((reaction_text) => {
        const emojisAndCount: [string, number][] = reaction_text.split('\n').map(line => findAndCountProminentEmoji(line));
        const emojis = emojisAndCount.map(e => { return { emoji: e[0], count: VariablePlaceholder() }});
        const counts = emojisAndCount.map(e => e[1]);
        return [emojis, counts];
    }, [reaction_text], [reactions, count], "parse");
    component.addConstraint(constraint);
});
```

`hd.For(each: Variable<T[]>, do: (value: T, index: Variable<number>) => void)` is like a for loop, but if the underlying variable changes (adding elements) the closure is executed again only for those added elements. Replacing an element also needs to re-run the effect as well as removing all previous constraints. Permutations are observed with the index variable. 
The closure passed to `hd.For` is also an effect, however, this example does not make use of this. It is inspired of Solid-Js' `<For>` component.


`addMethod` That means executing it does not change what variables are input/output. If `addMethod` could be made into an effect, it would make the api easier.

## Tags
This repository [https://git.app.uib.no/Janek.Greif/hottest/-/tree/main](https://git.app.uib.no/Janek.Greif/hottest/-/tree/main) has multiple tags that are referred to by this document. The tag names are noted in italic and surrounded by braces.

## Inspirations
There is a lot of inspiration from [Solid-Js](https://www.solidjs.com/), but also from [Marko](https://markojs.com/) and its new tag-api from version 6 (which is not yet released but here is a article about it [https://dev.to/ryansolid/introducing-the-marko-tags-api-preview-37o4](https://dev.to/ryansolid/introducing-the-marko-tags-api-preview-37o4)).
