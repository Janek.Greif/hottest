import * as hd from "hotdrink";
import {Component} from "solid-js";
import {bindBoolean, bindNumber, bindString, createComponent, createConstraint, varFomComp} from "./bindings";

bindNumber;
bindBoolean;
bindString;

const system = new hd.ConstraintSystem();

const SimpleArea: Component = () => {

    const comp = hd.component`
        var w = 100, h = 50, r = 2;

        constraint whr {
            m1(w, h -> r) => w/h;
            m2(w, r -> h) => w/r;
            m3(h, r -> w) => h*r;
        }
    `;

    system.addComponent(comp);

    const width = varFomComp<number>(comp, "w");
    const height = varFomComp<number>(comp, "h");
    const ratio = varFomComp<number>(comp, "r");

    return <>
        <label for="w">Width:</label>
        <input type="number" use:bindNumber={width} id="w"/>
        <br/>
        <label for="h">Height:</label>
        <input type="number" use:bindNumber={height} id="h"/>
        <br/>
        <label for="r">Ratio:</label>
        <input type="number" use:bindNumber={ratio} id="r"/>
    </>
}

const AdvancedArea: Component = () => {
    const comp = createComponent("comp");

    const locker = comp.createVariable(true);
    const width = comp.createVariable(100);
    const height = comp.createVariable(50);
    const ratio = comp.createVariable(2);
    const whrCons = createConstraint("whr");
    whrCons.addMethod(([l, w], [r, h]) => l ? [r, w / r] : [w / h, h], [locker, width], [ratio, height], "w->rh");
    whrCons.addMethod(([l, h], [r, w]) => l ? [r, h * r] : [w / h, w], [locker, height], [ratio, width], "h->rw");
    whrCons.addMethod(([l, r], [w, h]) => l ? [w, w / r] : [h * r, h], [locker, ratio], [width, height], "r->wh");

    comp.addConstraint(whrCons);
    comp.build(system);

    return <>
        <label for="w">Width:</label>
        <input type="number" use:bindNumber={width} id="w"/>
        <br/>
        <label for="h">Height:</label>
        <input type="number" use:bindNumber={height} id="h"/>
        <br/>
        <label for="r">Ratio:</label>
        <input type="number" use:bindNumber={ratio} id="r"/>
        <br/>
        <label for="l">Lock Ratio:</label>
        <input type="checkbox" use:bindBoolean={locker} id="l"/>
    </>
}

const App: Component = () => {
    const comp = createComponent("test");

    const name = comp.createVariable("Me");
    const obj = comp.createVariable({name: "Me", age: 42});
    const desc = comp.createVariable("Me is 42 years old");

    const objCons = createConstraint("obj");
    objCons.addMethod(([o]) => [`${o.name} is ${o.age} years old`], [obj], [desc]);

    const nameCons = createConstraint("name");
    nameCons.addMethod(([n], [o]) => [{...o, name: n}], [name], [obj]);

    comp.addConstraint(objCons);
    comp.addConstraint(nameCons);

    comp.build(system);

    return <>
        <SimpleArea/>
        <hr/>
        <AdvancedArea/>
        <hr/>
        <input use:bindString={name}/>
        <div use:bindString={desc}/>
    </>
}

export default App;