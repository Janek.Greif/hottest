import * as hd from "hotdrink";
import {JSX, onCleanup} from "solid-js";

type BinderParam<T> = Variable<T> | [Variable<T>] | [Variable<T>, (v: T) => boolean];
export type Binder<T> = (el: JSX.Element, variable: () => BinderParam<T>) => void;

declare module "solid-js" {
    namespace JSX {
        interface Directives {
            bindNumber: BinderParam<number>;
            bindString: BinderParam<string>;
            bindBoolean: BinderParam<boolean>;
        }
    }
}

export interface Variable<T> {
    v: hd.VariableReference<T>,

    set(t: T): void,

    set(update: (t: T) => T): void,

    get(): T,
}


export const bindNumber: Binder<number> = (el, variable) => {
    const vari = variable();
    let v: Variable<number>;
    let accepter: ((v: number) => boolean) | undefined = undefined;
    if (Array.isArray(vari)) {
        [v, accepter] = vari;
    } else {
        v = vari;
    }
    const value = v.v.value;
    if (value) {
        if (el instanceof HTMLInputElement) {
            let oldNum = parseFloat(el.value);
            el.addEventListener('input', function () {
                const n = parseFloat(this.value);
                if (!isNaN(n) && oldNum !== n && (accepter?.(n) ?? true)) {
                    oldNum = n;
                    value.set(n)
                }
            })
            value.subscribeValue((value: number) => el.value = "" + value);
        } else if (el instanceof HTMLElement) {
            value.subscribeValue((value: number) => el.textContent = "" + value);
        }
    }
}

export const bindString: Binder<string> = (el, variable) => {
    const vari = variable();
    let v: Variable<string>;
    let accepter: ((v: string) => boolean) | undefined = undefined;
    if (Array.isArray(vari)) {
        [v, accepter] = vari;
    } else {
        v = vari;
    }
    const value = v.v.value;
    if (value) {
        if (el instanceof HTMLInputElement) {
            el.addEventListener('input', function () {
                if (accepter?.(this.value) ?? true) {
                    value.set(this.value)
                }
            })
            value.subscribeValue((value: string) => el.value = value);
        } else if (el instanceof HTMLElement) {
            value.subscribeValue((value: string) => el.textContent = value);
        }
    }
}

export const bindBoolean: Binder<boolean> = (el, variable) => {
    const vari = variable();
    let v: Variable<boolean>;
    let accepter: ((v: boolean) => boolean) | undefined = undefined;
    if (Array.isArray(vari)) {
        [v, accepter] = vari;
    } else {
        v = vari;
    }
    const value = v.v.value;
    if (value) {
        if (el instanceof HTMLInputElement) {
            el.addEventListener('input', function () {
                if (accepter?.(this.checked) ?? true) {
                    value.set(this.checked)
                }
            })
            value.subscribeValue((value: boolean) => el.checked = value);
        } else if (el instanceof HTMLElement) {
            value.subscribeValue((value: boolean) => el.textContent = "" + value);
        }
    }
}

const newVar = <T>(v: hd.VariableReference<T>): Variable<T> => {
    return {
        v,
        set(p: T | ((t: T) => T)): void {
            if (p instanceof Function) {
                const old = v.value?.value!;
                v.value?.set(p(old));
            } else {
                v.value?.set(p);
            }
        },
        get(): T {
            return v.value?.value!;
        }
    }
}

export const varFomComp = <T>(comp: hd.Component, name: string): Variable<T> => {
    return newVar(comp.vs[name]!);
}

type VarOf<T> = { [K in keyof T]: Variable<T[K]> };

export interface HotConstraint {
    methods: hd.Method[],
    vars: Map<hd.VariableReference<any>, number>,
    name: string,

    addMethod<I extends Array<any>, O extends Array<any>>(fn: (i: [...I], o: [...O]) => [...O], vars: [...VarOf<I>], outs: [...VarOf<O>], name?: string): void;
}

export interface HotComponent {
    comp: hd.Component

    createVariable<T, >(val: T): Variable<T>;

    addConstraint(constraint: HotConstraint): void;

    build(system: hd.ConstraintSystem): void;
}

export const createConstraint = (name: string): HotConstraint => {
    return {
        methods: [],
        vars: new Map(),
        name,
        addMethod(
            fn,
            v,
            o,
            name?: string,
        ): void {

            const recolonizeVariable = (v: Variable<any>): number => {
                const idx = this.vars.get(v.v);
                if (idx !== undefined) {
                    return idx;
                } else {
                    const newIdx = this.vars.size;
                    this.vars.set(v.v, newIdx);
                    return newIdx;
                }
            };

            const vi = v.map(recolonizeVariable);
            const oi = o.map(recolonizeVariable);
            const sum = vi.length + oi.length;

            const m = new hd.Method(sum, vi, oi, [hd.maskNone], (...i: any) => {
                const r = fn(i, o.map(o => o.v.value?.value!) as any);
                if (r.length === 1) {
                    return r[0];
                } else {
                    return r;
                }
            }, name);
            this.methods.push(m);
        },
    };
}

export const createComponent = (name: string): HotComponent => {
    let varIdx = 0;
    return {
        comp: new hd.Component(name),

        createVariable<T, >(val: T): Variable<T> {
            const v = this.comp.emplaceVariable(`${name}var${varIdx}`, val);
            varIdx++;
            return newVar(v);
        },
        addConstraint(cons) {
            if (cons.methods.length > 0) {
                const test_consSpec = new hd.ConstraintSpec(cons.methods);
                const variables = [...cons.vars.entries()].sort((a, b) => a[1] - b[1]).map(e => e[0]);
                this.comp.emplaceConstraint(cons.name, test_consSpec, variables, false);
            }
        },
        build(system) {
            onCleanup(() => {
                system.removeComponent(this.comp);
            });
            system.addComponent(this.comp);
        },
    }
}